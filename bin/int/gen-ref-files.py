#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import annotations

import argparse
from pathlib import Path
from collections.abc import Sequence
from fnmatch import fnmatch


def main(argv: Sequence[str] | None = None) -> int:
    parser = argparse.ArgumentParser(description="Generate a amplicon ref-files json file from a amplicon ref-files tree.")
    parser.add_argument("-r", "--root", dest="root_dir", default="/vol/databases/reference-files/", help="Please specify the root directory where to find the amplicon's ref-files tree.")
    parser.add_argument("-o", "--out", dest="amplicon_ref_files_json_file", default="amplicon-ref-files.json", help="Please specify the name of the amplicon json ref-files name.")
    args = parser.parse_args(argv)

    fasta_exts = ["*.fasta,*.fna","*.fa"]
    taxonomy_ext = "*.txt"
    amplicon_ref_files_json_dict = {}
    
    root_directory = Path(args.root_dir)
    for path_object in root_directory.rglob('*'):
        if path_object.is_file():
            print(f"hi, I'm a file: {path_object}")
            print(f"Here's my basename: {path_object.name}")
        elif path_object.is_dir():
            continue

    return 0


if __name__ == "__main__":
    raise SystemExit(main())
